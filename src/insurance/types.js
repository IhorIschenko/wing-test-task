const prefix = "insurance";

export const CREATE_SPRINT_PURCHASE = `${prefix}/CREATE_SPRINT_PURCHASE`;
export const CREATE_ATT_PURCHASE = `${prefix}/CREATE_ATT_PURCHASE`;

export const SPRINT_INSURANCE_PLAN = `${prefix}/SPRINT_INSURANCE_PLAN`;
