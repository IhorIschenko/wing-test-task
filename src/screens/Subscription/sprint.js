import React from "react";
import SubscriptionSwitcher from "subscriptions/SubscriptionSwitcher";
import Box from "common/Box";
import Link from "common/Link";
import * as routes from "app/routes";
import add_green_circle from "common/img/add_green_circle.svg";
import styles from "./Subscription.module.css";

export const SubscriptionScreen = ({ subId, subscription }) => {
  return (
    <div className="Subscription">
      <SubscriptionSwitcher
        sprintSubId={subId}
        sprintRoute={routes.sprintSubscription}
        attRoute={routes.attSubscription}
      />
      {subscription && !subscription.has_existing_insurance_contract ? (
        <div className={styles.linkList}>
          <Link
            className={styles.subscriptionLink}
            to={routes.sprintInsurance(
              subId,
              subscription.device_specs,
              subscription.has_existing_insurance_contract,
              subscription.sprint_status,
              subscription
            )}
          >
            <Box>
              <img src={add_green_circle} alt="" />
              Insurance
            </Box>
          </Link>
        </div>
      ) : null}
    </div>
  );
};

export default SubscriptionScreen;
