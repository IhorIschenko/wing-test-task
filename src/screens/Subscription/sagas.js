import { put, call } from "redux-saga/effects";
import { authenticateUser } from "auth/sagas";
import { setRedirect } from "auth/actions";
import { sprintSubscription, attSubscription } from "app/routes";

export function* sprintSubscriptionNavigate({ subId, subscription }) {
  yield put(setRedirect(sprintSubscription(subId, subscription)));
  yield call(authenticateUser);
}

export function* attSubscriptionNavigate({ subId, subscription }) {
  yield put(setRedirect(attSubscription(subId, subscription)));
  yield call(authenticateUser);
}
