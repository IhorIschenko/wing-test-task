import React from "react";
import styles from "./InsurancePlan.module.css";
import * as routes from "app/routes";
import Back from "common/Back";

const AttInsurancePlan = ({ subId, subscription }) => {
  return (
    <div className={styles.container}>
      <div className={styles.Back}>
        <Back
          to={routes.attInsurance(
            subId,
            subscription.device_specs,
            subscription.has_existing_insurance_contract,
            subscription.att_status,
            subscription
          )}
        />
      </div>
      <h2>Select an insurance plan</h2>
      <div className={styles.card_container}>
        <div className={styles.plan_card}>
          <h3 className={styles.card_header}>Wing Extended Warranty</h3>
          <div className={styles.price_block}>
            <p className={styles.price_text}>Starting at</p>
            <p className={styles.price}>$0.96</p>
          </div>
          <div className={styles.button_block}>
            <p>Terms, fees, and more info</p>
            <button className={styles.card_button_select}>Select</button>
          </div>
          <div className={styles.card_description_block}>
            <p>Protects your device against</p>
            <ul>
              <li>
                Malfunction (after the original manufacturer's warranty expires)
              </li>
            </ul>
            <hr />
            <div className={styles.bottom_info}>
              <p>Repair deductible: $24.50</p>
              <p>Replacement deductible: $49.00</p>
            </div>
          </div>
        </div>
        <div className={styles.plan_card}>
          <h3 className={styles.card_header}>Wing Extended Warranty</h3>
          <div className={styles.price_block}>
            <p className={styles.price_text}>Starting at</p>
            <p className={styles.price}>$0.96</p>
          </div>
          <div className={styles.button_block}>
            <p>Terms, fees, and more info</p>
            <button className={styles.card_button_select}>Select</button>
          </div>
          <div className={styles.card_description_block}>
            <p>Protects your device against</p>
            <ul>
              <li>
                Malfunction (after the original manufacturer's warranty expires)
              </li>
            </ul>
            <hr />
            <div className={styles.bottom_info}>
              <p>Repair deductible: $24.50</p>
              <p>Replacement deductible: $49.00</p>
            </div>
          </div>
        </div>
        <div className={styles.plan_card}>
          <h3 className={styles.card_header}>Wing Extended Warranty</h3>
          <div className={styles.price_block}>
            <p className={styles.price_text}>Starting at</p>
            <p className={styles.price}>$0.96</p>
          </div>
          <div className={styles.button_block}>
            <p>Terms, fees, and more info</p>
            <button className={styles.card_button_select}>Select</button>
          </div>
          <div className={styles.card_description_block}>
            <p>Protects your device against</p>
            <ul>
              <li>
                Malfunction (after the original manufacturer's warranty expires)
              </li>
            </ul>
            <hr />
            <div className={styles.bottom_info}>
              <p>Repair deductible: $24.50</p>
              <p>Replacement deductible: $49.00</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default AttInsurancePlan;
