import { put, call } from "redux-saga/effects";
import { authenticateUser } from "auth/sagas";
import { setRedirect } from "../../auth/actions";
import { sprintInsurancePlan, attInsurancePlan } from "../../app/routes";

export function* sprintInsurancePlanNavigate(subId, sku, subscription) {
  yield put(setRedirect(sprintInsurancePlan(subId, sku, subscription)));
  yield call(authenticateUser);
}

export function* attInsurancePlanNavigate(subId, sku, subscription) {
  yield put(setRedirect(attInsurancePlan(subId, sku, subscription)));
  yield call(authenticateUser);
}
