import React from "react";
import * as routes from "app/routes";
import Back from "common/Back";
import styles from "./Insurance.module.css";
import SubscriptionSwitcher from "subscriptions/SubscriptionSwitcher";
import add_green_circle from "common/img/add_green_circle.svg";
import Box from "common/Box";
import Link from "common/Link";

import InsuranceInfo from "insurance/InsuranceInfo/att";

const Insurance = ({
  subId,
  sku,
  hasExistingInsuranceContract,
  status,
  subscription,
}) => (
  <div>
    <SubscriptionSwitcher
      attSubId={subId}
      sprintRoute={routes.sprintInsurance}
      attRoute={routes.attInsurance}
    />
    <div className={styles.Back}>
      <Back to={routes.attSubscription(subId, subscription)} />
    </div>
    {!hasExistingInsuranceContract && status === "active" ? (
      <div className={styles.linkList}>
        <Link
          className={styles.subscriptionLink}
          to={routes.attInsurancePlan(subId, sku, subscription)}
        >
          <Box>
            <img src={add_green_circle} alt="" />
            Select Plan
          </Box>
        </Link>
      </div>
    ) : (
      <>
        <h1>Your Device Protection Details</h1>
        <InsuranceInfo attSubId={subId} />
      </>
    )}
  </div>
);

export default Insurance;
