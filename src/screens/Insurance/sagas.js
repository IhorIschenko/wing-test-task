import { put, call } from "redux-saga/effects";
import { authenticateUser } from "auth/sagas";
import { setRedirect } from "auth/actions";
import { sprintInsurance, attInsurance } from "app/routes";

export function* sprintInsuranceNavigate({
  subId,
  sku,
  has_existing_insurance_contract,
  status,
  subscription,
}) {
  yield put(
    setRedirect(
      sprintInsurance(
        subId,
        sku,
        has_existing_insurance_contract,
        status,
        subscription
      )
    )
  );
  yield call(authenticateUser);
}

export function* attInsuranceNavigate({
  subId,
  sku,
  has_existing_insurance_contract,
  status,
  subscription,
}) {
  yield put(
    setRedirect(
      attInsurance(
        subId,
        sku,
        has_existing_insurance_contract,
        status,
        subscription
      )
    )
  );
  yield call(authenticateUser);
}
